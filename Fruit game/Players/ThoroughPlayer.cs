﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fruit_game.Players
{
    public class ThoroughPlayer : GenericPlayer
    {
        private int prevWeight = 0;
        
        public ThoroughPlayer(string name) : base(name)
        {
            this.name = name;
        }
        
        public override int TryToGuess()
        {
            int weight = 0;
            if (Constants.MINWEIGHT > prevWeight)
            {
                prevWeight = Constants.MINWEIGHT - 1;
            }

            weight = ++prevWeight;

            if (!prevWeights.ContainsKey(weight))
                    lock (prevWeights)
                    {
                        prevWeights.Add(weight, GameTimer.GetElapsedMilliseconds());
                    }

            return weight;
        }
    }
}
