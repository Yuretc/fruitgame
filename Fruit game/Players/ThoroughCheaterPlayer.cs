﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Fruit_game.Players
{
    public class ThoroughCheaterPlayer : GenericCheater
    {
        private int prevWeight = 0;

        public ThoroughCheaterPlayer(string name) : base(name)
        {
            this.name = name;
        }


        public override int TryToGuess()
        {
            int weight = 0;
            while (otherPlayersWeights.ContainsKey(weight) || weight == 0)
            {
                if (Constants.MINWEIGHT > prevWeight)
                {
                    prevWeight = Constants.MINWEIGHT - 1;
                }

                weight = ++prevWeight;
            }
            if (!prevWeights.ContainsKey(weight))
                    lock (prevWeights)
                    {
                            prevWeights.Add(weight, GameTimer.GetElapsedMilliseconds());
                    }

            return weight;
        }
        
    }
}
