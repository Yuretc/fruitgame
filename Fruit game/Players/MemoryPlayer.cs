﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Fruit_game.Players;

namespace Fruit_game.Players
{
    public class MemoryPlayer : GenericPlayer
    {
        public MemoryPlayer(string name) : base(name)
        {
            this.name = name;
        }

        public override int TryToGuess()
        {
            int weight = 0;
            Random r = new Random();
            while(prevWeights.ContainsKey(weight) || weight == 0)
                weight = r.Next(Constants.MINWEIGHT, Constants.MAXWEIGHT);

            if (!prevWeights.ContainsKey(weight))
                    lock (prevWeights)
                    {
                        prevWeights.Add(weight, GameTimer.GetElapsedMilliseconds());
                    }

            return weight;
        }        
    }
}
