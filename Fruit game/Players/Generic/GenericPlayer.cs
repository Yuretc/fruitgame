﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace Fruit_game.Players
{
    abstract public class GenericPlayer
    {
        protected Dictionary<int, int> prevWeights = new Dictionary<int, int>();

        protected string name = String.Empty;


        public GenericPlayer(string name)
        {
            this.name = name;
        }        
        

        public virtual string Name
        {
            get
            {
                return name;
            }
        }

        public Dictionary<int, int> PrevWeights
        {
            get
            {
                return prevWeights;
            }
        }


        abstract public int TryToGuess();


        public void Wait(int milliseconds)
        {
            Thread.Sleep(milliseconds);
        }

    }
}
