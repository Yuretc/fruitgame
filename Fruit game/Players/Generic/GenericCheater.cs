﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fruit_game.Players
{
    abstract public class GenericCheater : GenericPlayer
    {
        protected Dictionary<int, int> otherPlayersWeights = new Dictionary<int, int>();

        public GenericCheater(string name) : base(name)
        {
            this.name = name;
        }


        public void SetOtherPlayersWeights(Dictionary<int, int> otherPlayersWeights)
        {
            if (otherPlayersWeights.Count > 0)
            {
                foreach (var key in otherPlayersWeights.Keys)
                {
                    if (!this.otherPlayersWeights.ContainsKey(key))
                    {
                        this.otherPlayersWeights.Add(key, otherPlayersWeights[key]);
                    }
                }
            }
        }
    }
}
