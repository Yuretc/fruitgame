﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace Fruit_game.Players
{
    public class RandomPlayer : GenericPlayer
    {
        public RandomPlayer(string name) : base(name)
        {
            this.name = name;
        }

        public override int TryToGuess()
        {
            int weight = 0;
            Random r = new Random();

            weight = r.Next(Constants.MINWEIGHT, Constants.MAXWEIGHT);
            if(!prevWeights.ContainsKey(weight))
                    lock (prevWeights)
                    {
                            prevWeights.Add(weight, GameTimer.GetElapsedMilliseconds());
                    }

            return weight;
        }
    }
}
