﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Fruit_game.Players
{
    public class CheaterPlayer : GenericCheater
    {
        public CheaterPlayer(string name) : base(name)
        {
            this.name = name;
        }

        public override int TryToGuess()
        {            
            int weight = 0;
            Random r = new Random();
            while (otherPlayersWeights.ContainsKey(weight) || weight == 0)
                weight = r.Next(Constants.MINWEIGHT, Constants.MAXWEIGHT);

            if (!prevWeights.ContainsKey(weight))
                    lock (prevWeights)
                    {
                        prevWeights.Add(weight, GameTimer.GetElapsedMilliseconds());
                    }

            return weight;
        }

    }
}
