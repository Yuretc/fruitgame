﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Fruit_game.Players;

namespace Fruit_game
{
    public static class PlayerCreator
    {
        public enum PlayerType
        {
            Random = 1,
            Memory = 2,
            Thorough = 3,
            Cheater = 4,
            ThoroughCheater = 5
        }

        public static GenericPlayer CreatePlayer(string name, PlayerType type)
        {
            switch (type)
            {
                case PlayerType.Random:
                   return  new RandomPlayer(name);
                case PlayerType.Memory:
                    return new MemoryPlayer(name);
                case PlayerType.Thorough:
                    return new ThoroughPlayer(name);
                case PlayerType.Cheater:
                    return new CheaterPlayer(name);
                case PlayerType.ThoroughCheater:
                    return new ThoroughCheaterPlayer(name);
                default:
                    Console.WriteLine("Not implemented player type!");
                    throw new NotImplementedException();
            }            
        }        
    }
}
