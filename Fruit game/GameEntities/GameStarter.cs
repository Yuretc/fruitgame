﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace Fruit_game
{
    class GameStarter
    {
        private GameManager gM = new GameManager();     

        public GameStarter()
        {
            Init();
        }


        public void Init()
        {
            gM = new GameManager();
        pickplayer:
            Console.WriteLine("How many players do you want? (Min: " + Constants.MINALLOWEDPLAYERS + " , Max: " + Constants.MAXALLOWEDPLAYERS + ")");

            int numberPlayers = 0;
            var result = int.TryParse(Console.ReadLine(), out numberPlayers);

            Console.WriteLine();

            if (!result || numberPlayers < Constants.MINALLOWEDPLAYERS || numberPlayers > Constants.MAXALLOWEDPLAYERS)
            {
                BadInput();
                goto pickplayer;
            }

            Dictionary<string, int> playersList = new Dictionary<string, int>();

            Console.WriteLine("Possible types of players: \n 1.Random player \n 2.Memory player \n 3.Thorough player \n 4.Cheater player \n 5.Thorough cheater player");
            Console.WriteLine("Enter name and type of player (Example: Jeka 2)");
            Console.WriteLine();

            for (int i = 1; i <= numberPlayers; i++)
            {
            enterplayer:
                Console.WriteLine("Enter " + i.ToString() + " player");

                var userEntry = Console.ReadLine();
                if (!userEntry.Contains(" "))
                {
                    BadInput();
                    goto enterplayer;
                }
                var name = userEntry.Substring(0, userEntry.IndexOf(' '));

                int type = 0;
                try
                {
                    type = int.Parse(userEntry.Substring(name.Length + 1));
                }
                catch
                {
                    BadInput();
                    goto enterplayer;
                }

                if (!playersList.ContainsKey(name))
                    playersList.Add(name, type);
                else
                {
                    Console.WriteLine("Player " + name + " already exists!");
                    BadInput();
                    goto enterplayer;
                }
            }
            gM.AddPlayers(playersList);
        }
        
        private void BadInput()
        {
            Console.WriteLine("Bad input! Try again!");
            Console.WriteLine();
        }

        public void StartGame()
        {
            Random r = new Random();
            var realWeight = r.Next(Constants.MINWEIGHT, Constants.MAXWEIGHT);

            Console.WriteLine();
            Console.WriteLine("Real weight is " + realWeight);

            gM.Start(realWeight);
        }
    }
}
