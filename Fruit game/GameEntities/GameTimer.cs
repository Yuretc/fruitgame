﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fruit_game
{
    public static class GameTimer
    {
        static private Stopwatch watch = new Stopwatch();


        static public void StopTimer()
        {
            if (!ReachedMaxAlowedTime() && watch.IsRunning)
            {
                watch.Stop();
            }
        }

        static public void Start()
        {
            watch.Reset();
            watch.Start();
        }

        static public int GetElapsedMilliseconds()
        {
            return (int)watch.ElapsedMilliseconds;
        }

        static public bool ReachedMaxAlowedTime()
        {
            return watch.ElapsedMilliseconds >= Constants.MAXTIMEALOWED;
        }

    }
}
