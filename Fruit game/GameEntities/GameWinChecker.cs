﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fruit_game
{
    class GameWinChecker
    {

        private bool win = false;
        private int realWeight = 0;
        private string winnerName = String.Empty;
        private int winnerWeight = 0;
        private int winnerTime = 0;
        

        public GameWinChecker()
        {
        }
        
        public bool Win
        {
            get { return win; }
            set { win = value; }
        }

        public int RealWeight
        {
            get { return realWeight; }
            set { realWeight = value; }
        }

        public string WinnerName
        {
            get { return winnerName; }
        }

        public int WinnerTime
        {
            get { return winnerTime; }
        }

        public int GuessedWinnerWeight
        {
            get { return winnerWeight; }
        }
        

        public bool CheckIfWeightSameAsRealWeight(int guessedWeight)
        {
            if (guessedWeight == realWeight)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool CheckGameEnds(int totalAttempts)
        {
            if (!win && !ReachedMaxAllowedAttempts(totalAttempts) && !GameTimer.ReachedMaxAlowedTime())
                return false;
            else return true;
        }

        public bool ReachedMaxAllowedAttempts(int totalAttempts)
        {
            return totalAttempts >= Constants.MAXATTEMPTS;
        }

        public void SetWinner(string winnersName, int winnersWeight)
        {
            if (!win)
            {
                winnerName = winnersName;
                winnerWeight = winnersWeight;
                winnerTime = GameTimer.GetElapsedMilliseconds();
                win = true;
            }
        }

        public void IfNooneWon(Dictionary<string, Dictionary<int, int>> playerListForCheck)
        {
            if (!win)
            {
                GetClosestPlayerTillNeededValue(playerListForCheck);
            }
        }


        private void GetClosestPlayerTillNeededValue(Dictionary<string, Dictionary<int, int>> playerListForCheck)
        {
            string winnersName = String.Empty;
            int winnersWeight = 0;
            int resultWeightDiff = Constants.MAXWEIGHT;
            int firstWeightTime = 0;

            int maxMillisecondsValue = 0;

            foreach (var player in playerListForCheck.Keys)
            {
                foreach (var playerWeight in playerListForCheck[player].Keys)
                {
                    int playerDiff = Math.Abs(playerWeight - realWeight);
                    var playerWeightTime = playerListForCheck[player][playerWeight];

                    if(maxMillisecondsValue < playerWeightTime)
                        maxMillisecondsValue = playerWeightTime;

                    if (playerDiff < resultWeightDiff)
                    {
                        resultWeightDiff = playerDiff;
                        SetNewValues(player, playerWeight, playerWeightTime, out winnersName, out winnersWeight, out firstWeightTime);

                    }
                    else if (playerDiff == resultWeightDiff)
                    {
                        if (playerWeightTime.CompareTo(firstWeightTime) < 0)
                        {
                            SetNewValues(player, playerWeight, playerWeightTime, out winnersName, out winnersWeight, out firstWeightTime);
                        }
                    }
                }
            }

            winnerName = winnersName;
            winnerWeight = winnersWeight;
            winnerTime = maxMillisecondsValue;
        }

        private void SetNewValues(string playerName, int playerWeight, int playerDt,
            out string resultName, out int resultWeight, out int resultDt)
        {
            resultName = playerName;
            resultWeight = playerWeight;
            resultDt = playerDt;
        }
    }
}
