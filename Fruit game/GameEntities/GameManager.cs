﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Fruit_game.Players;
using System.Timers;
using System.Diagnostics;

namespace Fruit_game
{
    public class GameManager
    {
        private List<Thread> playerThreads = new List<Thread>();
        private List<GenericPlayer> playersList = new List<GenericPlayer>();
        private Dictionary<string, int> playersAttempts = new Dictionary<string, int>();
        private object lockObject = new object();
        private GameWinChecker gameWinChecker;

        public GameManager()
        {
            gameWinChecker = new GameWinChecker();

            playersList.Clear();
            playerThreads.Clear();
            playersAttempts.Clear();
        }


        public void AddPlayers(Dictionary<string, int> requestedPlayersWithType)
        {
            foreach (var playerName in requestedPlayersWithType.Keys)
            {
                playersList.Add(PlayerCreator.CreatePlayer(playerName, (PlayerCreator.PlayerType)requestedPlayersWithType[playerName]));
            }
            foreach (var playerName in requestedPlayersWithType.Keys)
            {
                playersAttempts.Add(playerName, 0);
            }
        }

        public void Start(int realWeight)
        {
            Thread.Sleep(10);

            gameWinChecker.RealWeight = realWeight;

            GameTimer.Start();

            foreach (var player in playersList)
            {
                Thread t = new Thread(() => StartGuessing(player));
                playerThreads.Add(t);
            }


            playerThreads.ForEach(thread => thread.Start());

            GetAliveThreads().ForEach(thread => thread.Join());

            GameTimer.StopTimer();

            gameWinChecker.IfNooneWon(MakePlayerListForChecker());

            DeclareResults();
        }


        private void StartGuessing(GenericPlayer player)
        {
            var playerName = player.Name;
            int probableWeight = 0;
            bool wonResult = false;

            while (!gameWinChecker.CheckGameEnds(GetTotalAmountAttempts()))
            {
                if (player is GenericCheater)
                {
                    (player as GenericCheater).SetOtherPlayersWeights(MakePrevWeightsForCheater());
                }

                lock (player)
                {
                    probableWeight = player.TryToGuess();
                }

                wonResult = gameWinChecker.CheckIfWeightSameAsRealWeight(probableWeight);

                var smbdyWon = gameWinChecker.CheckGameEnds(GetTotalAmountAttempts());

                if (!wonResult && !smbdyWon)
                {
                    AddAttemptToPlayer(playerName);
                    player.Wait(Math.Abs(gameWinChecker.RealWeight - probableWeight));
                }
                else if (!wonResult && smbdyWon)
                {
                    break;
                }
                else
                {
                    lock (gameWinChecker)
                    {
                        AddAttemptToPlayer(playerName);
                        gameWinChecker.SetWinner(playerName, probableWeight);
                    }
                }
            }
        }

        private void DeclareResults()
        {
            Console.WriteLine();
            if (gameWinChecker.Win)
                Console.WriteLine("Player " + gameWinChecker.WinnerName + " win!");
            else
                Console.WriteLine("Player " + gameWinChecker.WinnerName + " with " + gameWinChecker.GuessedWinnerWeight + " was closest to real weight");

            Console.WriteLine("Total amount of all players attempts = " + GetTotalAmountAttempts());
            Console.WriteLine("Milliseconds passed - " + gameWinChecker.WinnerTime);
            Console.WriteLine();
        }

        private void AddAttemptToPlayer(string playerName)
        {
            lock(playersAttempts)
            {
                if(!gameWinChecker.ReachedMaxAllowedAttempts(GetTotalAmountAttempts()))
                    ++playersAttempts[playerName];
            }
        }


        private int GetTotalAmountAttempts()
        {
            lock (playersAttempts)
               return playersAttempts.Select(r => r.Value).Sum();
        }        

        private List<Thread> GetAliveThreads()
        {
            return playerThreads.Where(thread => thread.IsAlive).ToList();
        }

        private Dictionary<int, int> MakePrevWeightsForCheater()
        {
            Dictionary<int, int> listForCheaters = new Dictionary<int, int>();

            foreach (var player in playersList)
            {
                lock(player.PrevWeights)
                {
                    player.PrevWeights.Where(r => !listForCheaters.ContainsKey(r.Key)).ToList()
                        .ForEach(r => listForCheaters.Add(r.Key, r.Value));
                }
            }

            return listForCheaters;
        }

        private Dictionary<string, Dictionary<int, int>> MakePlayerListForChecker()
        {
            Dictionary<string, Dictionary<int, int>> playerListForChecker = new Dictionary<string, Dictionary<int, int>>();

            foreach (var player in playersList)
            {
                playerListForChecker.Add(player.Name, player.PrevWeights);
            }

            return playerListForChecker;
        }

    }
}
