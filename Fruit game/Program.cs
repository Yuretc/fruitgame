﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fruit_game
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Wanna try to guess weight of basket?");
            Console.WriteLine("1. Yes");
            Console.WriteLine("2. NO!");
            var choise = Console.ReadLine();
            Console.WriteLine("");

            switch (choise.ToLower())
            {
                case "yes":
                case "1":
                    again:
                    GameStarter gs = new GameStarter();
                    gs.StartGame();
                    if (PlayAgain())
                        goto again;                    
                    break;

                case "no":
                case "2":
                    Environment.Exit(0);
                    break;
            }
        }

        private static bool PlayAgain()
        {
            Console.WriteLine("Wanna play again?");
            Console.WriteLine("1. Yes");
            Console.WriteLine("2. NO!");
            var choise = Console.ReadLine();
            Console.WriteLine("");

            switch (choise.ToLower())
            {
                case "yes":
                case "1":
                    return true;

                case "no":
                case "2":
                    return false;

                default: return false;
            }
        }
    }
}
