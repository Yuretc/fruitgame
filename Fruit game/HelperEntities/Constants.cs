﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fruit_game
{
    public static class Constants
    {
        public const int MINWEIGHT = 40;
        public const int MAXWEIGHT = 140;
        public const int MINALLOWEDPLAYERS = 2;
        public const int MAXALLOWEDPLAYERS = 8;
        public const int MAXATTEMPTS = 100;
        public const int MAXTIMEALOWED = 1500;
    }
}
